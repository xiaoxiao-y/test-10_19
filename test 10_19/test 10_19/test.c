#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//int main()
//{
//	char arr[14] = { 0 };
//	int N = 0;
//	scanf("%d", &N);
//	int k = 0;//记录逗号的个数
//	int i = 0;//数组下标
//	while (N)
//	{
//		if (k != 0 && k % 3 == 0)
//		{
//			arr[i++] = ',';
//		}
//		arr[i++] = N % 10 + '0';
//		N = N / 10;
//		k++;
//	}
//	for(i--; i >= 0; i--)
//	{
//		printf("%c", arr[i]);
//	}
//
//	return 0;
//}
//int is_exist(char ch, char arr[])
//{
//	int i = 0;
//	while (arr[i])
//	{
//		if (ch == arr[i])
//		{
//			return 1;
//		}
//		i++;
//	}
//	return 0;
//}
//int main()
//{
//	//字符长度不超过100，最长100
//	char arr1[101] = { 0 };
//	char arr2[101] = { 0 };
//	//scanf不允许%s输入，所以使用函数gets()
//	gets(arr1);
//	gets(arr2);
//	int i = 0;
//	while (arr1[i])//到\0跳出
//	{
//		//if (is_exist(arr1[i], arr2) == 0)//判断arr1到底在arr2中有没有存在要遍历arr1
//		//{
//		//	printf("%c", arr1[i]);
//		//}
//		//i++;
//		//库函数strchr//在一个字符串中找一个字符是否存在。头文件string.h
//		if (strchr(arr2, arr1[i]) == NULL)
//		{
//			printf("%c", arr1[i]);
//
//		}
//		i++;
//	}
//	return 0;
//}
//动态内存管理
#include<limits.h>
#include<stdlib.h>//malloc的头文件
int main()
{
	//malloc的返回类型为void*,需强制转换（使用者自行决定）
	//int* p = (int*)malloc(10 * sizeof(int));//没有申请失败
	int* p = (int*)malloc(INT_MAX*4);//申请太大会失败了
	if (p == NULL)
	{
		perror("malloc");//都要加""
		return 1;
	}
	//如果后面还再使用就会出现，对NULL指针进行解引用操作
	//p如果申请成功，相当于指向起始位置
	return 0;
}